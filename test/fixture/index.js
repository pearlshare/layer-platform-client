module.exports = {
  res: {
    createConversation: require("./create_conversation_res"),
    updateConversation: require("./update_conversation_res"),
    sendMessage: require("./send_message_res")
  },
  req: {
    createConversation: require("./create_conversation_req"),
    updateConversation: require("./update_conversation_req"),
    sendMessage: require("./send_message_req")
  }
};
