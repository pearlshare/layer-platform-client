var layerClient = require("../");
var expect = require("expect.js");
var nock = require("nock");
var fixtures = require("./fixture");

var config = {
  appId: "LAYER_APP_ID",
  enabled: false, // Disabled for testing
  reqTimeout: 5 * 1000,
  platformKey: "LAYER_PLATFORM_KEY"
};


describe("layer-platform-client", function () {

  describe("configuration", function () {
    it("should return an object if configuation is correct", function () {
      expect(layerClient(config)).to.be.a("object");
    });

    it("should throw an error if no configuration object is given", function () {
      try {
        layerClient();
        expect().fail("should build the layer client");
      } catch (err) {
        expect(err).to.be.an(Error);
        expect(err.message).to.match(/config/);
      }
    });

    it("should throw an error if no appId is given", function () {
      try {
        layerClient({
          enabled: false,
          platformKey: config.platformKey
        });
        expect().fail("should build the mandrill client");
      } catch (err) {
        expect(err).to.be.an(Error);
        expect(err.message).to.match(/appId/);
      }
    });

    it("should throw an error if no platformKey is given", function () {
      try {
        layerClient({
          appId: config.appId,
          enabled: false
        });
        expect().fail("should build the mandrill client");
      } catch (err) {
        expect(err).to.be.an(Error);
        expect(err.message).to.match(/platformKey/);
      }
    });
  });

  describe("makeRequest", function () {
    var layer = layerClient(config);

    it("should throw an error if inappropriate params are given", function () {
      config.enabled = true;

      var err;

      try {
        layer.makeRequest({
          path: "/bar"
        });
      } catch (e) {
        err = e;
      }
      expect(err);
    });

    it("should resolve to empty array if layer not enabled", function () {
      config.enabled = false;

      layer.makeRequest({
        method: "get",
        path: "/bar"
      }).then(function(res) {
        expect(res.body).to.eql(undefined);
      });
    });

    it("should resolve with the layer response if layer is enabled", function () {
      config.enabled = true;

      var mockResponse = {
        id: "fish"
      };

      // Nock out layer
      nock(layer.layerUrl)
        .get("/apps/LAYER_APP_ID/bar")
        .reply(201, mockResponse);

      return layer.makeRequest({
        method: "get",
        path: "bar"
      }).then(function(res) {
        expect(res.body.id).to.eql(mockResponse.id);
      });
    });

    it("should not throw an error if the statusCode is 400", function() {
      config.enabled = true;

      // Nock out layer
      nock(layer.layerUrl)
        .get("/apps/LAYER_APP_ID/bar")
        .reply(400, {});

      return layer.makeRequest({
        method: "get",
        path: "bar"
      }).then(function(res) {
        expect(res.statusCode).to.eql(400);
        expect(res.body).to.be.an("object");
      });
    });
  });

  describe("createConversation", function () {
    var layer = layerClient(config);

    it("should resolve to empty array if layer not enabled", function () {
      config.enabled = false;

      var err;

      try {
        layer.createConversation({
          participants: ["1234", "5678"]
        });
      } catch (e) {
        err = e;
      }
      expect(err);
    });

    it("should make a request if layer is enabled", function () {
      config.enabled = true;

      var mockResponse = fixtures.res.createConversation;

      // Nock out layer messages
      nock(layer.layerUrl)
        .post("/apps/LAYER_APP_ID/conversations")
        .reply(201, mockResponse);

      return layer.createConversation(fixtures.req.createConversation).then(function(res) {
        expect(res.body).to.be.a("object");
        expect(res.body.id).to.eql(mockResponse.id);
      });
    });

    it("should throw an error if the message body isn't valid", function () {
      config.enabled = true;
      var err;
      try {
        layer.createConversation({});
      } catch (e) {
        err = e;
      }
      expect(err);
      expect(err.message).to.eql("layer conversation body not valid");
    });
  });

  describe("updateConversation", function () {
    var layer = layerClient(config);

    it("should resolve to empty array if layer not enabled", function () {
      config.enabled = false;

      var err;

      try {
        layer.updateConversation(1, fixtures.req.updateConversation);
      } catch (e) {
        err = e;
      }
      expect(err);
    });

    it("should make a request if layer is enabled", function () {
      config.enabled = true;

      var mockResponse = fixtures.res.updateConversation;

      // Nock out layer messages
      nock(layer.layerUrl)
        .patch("/apps/LAYER_APP_ID/conversations/1")
        .reply(200, mockResponse);

      return layer.updateConversation(1, fixtures.req.updateConversation).then(function(res) {
        expect(res.body).to.be.a("object");
        expect(res.body.id).to.eql(mockResponse.id);
      });
    });

    it("should throw an error if the message body isn't valid", function () {
      config.enabled = true;

      var err;
      try {
        layer.updateConversation(1, undefined);
      } catch (e) {
        err = e;
      }
      expect(err);
      expect(err.message).to.eql("layer updateConversation body not valid");
    });
  });

  describe("sendMessage", function () {
    var layer = layerClient(config);

    it("should resolve to empty array if layer not enabled", function () {
      config.enabled = false;

      var err;

      try {
        layer.sendMessage(1, fixtures.req.sendMessage);
      } catch (e) {
        err = e;
      }
      expect(err);
    });

    it("should make a request if layer is enabled", function () {
      config.enabled = true;

      var mockResponse = fixtures.res.sendMessage;

      // Nock out layer messages
      nock(layer.layerUrl)
        .post("/apps/LAYER_APP_ID/conversations/1/messages")
        .reply(201, mockResponse);

      return layer.sendMessage(1, fixtures.req.sendMessage).then(function(res) {
        expect(res.body).to.be.a("object");
        expect(res.body.id).to.eql(mockResponse.id);
      });
    });

    it("should throw an error if the message body isn't valid", function () {
      config.enabled = true;
      var err;
      try {
        layer.sendMessage(1, {});
      } catch (e) {
        err = e;
      }
      expect(err);
      expect(err.message).to.eql("layer sendMessage body not valid");
    });
  });
});
