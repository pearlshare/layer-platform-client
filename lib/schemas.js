var schemajs = require("schemajs");

var createConversationValidator = schemajs.create({
  participants: {
    type: "array",
    required: true
  },
  distinct: {
    type: "boolean",
    default: true
  },
  metadata: {
    type: "object",
    allownull: true,
    schema: {
      "background_color": {
        type: "string",
        allownull: true
      },
      title: {
        type: "string",
        allownull: true
      }
    }
  }
});


var updateConversationValidator = schemajs.create({
  type: "array",
  schema: {
    operation: {
      type: "string",
      required: true,
      properties: {
        in: ["add", "remove", "set"]
      }
    },
    property: {
      type: "string",
      required: true
    },
    value: {
      type: "string",
      required: true
    }
  }
});

var sendMessageValidator = schemajs.create({
  "sender": {
    type: "object",
    required: true,
    schema: {
      name: {
        description: "When sending from a non human give a name",
        type: "string",
        allownull: true
      },
      "user_id": {
        description: "When sending from a human give their id",
        type: "string",
        filters: "toString",
        allownull: true
      }
    }
  },
  "parts": {
    type: "array",
    required: true,
    schema: {
      body: {
        description: "Message contents either text or base64",
        type: "string",
        required: true
      },
      "mime_type": {
        description: "Mime type of the message being sent",
        type: "string",
        allownull: true,
        default: "text/plain"
      },
      encoding: {
        description: "If sending an image set to base64, otherwise blank",
        type: "string",
        allownull: true,
        properties: {
          in: ["base64"]
        }
      }
    }
  },
  "notification": {
    type: "object",
    allownull: true,
    schema: {
      text: {
        type: "string"
      },
      sound: {
        type: "string"
      }
    }
  }
});

module.exports = {
  createConversation: createConversationValidator,
  updateConversation: updateConversationValidator,
  sendMessage: sendMessageValidator
};
