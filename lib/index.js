/*

  Make requests to the layer platform API

  https://developer.layer.com/docs/platform

 */
var Bluebird = require("bluebird");
var got = require("got");
var schemas = require("./schemas");
var util = require("util");

function combine(dict1, dict2) {
  for (var attr in dict2) {
    dict1[attr] = dict2[attr];
  }

  return dict1;
}

/**
 * ValidationError
 * @param {String}  message     error messages
 * @param {Object}  errors      any form errors to attach the error
 * @returns {Error} validation errors
 */
function ValidationError (message, errors) {
  Error.apply(this, arguments);
  this.name = "layer-platform-client validation error";
  this.message = message;
  this.errors = errors;
  return this;
}

util.inherits(ValidationError, Error);

/**
 * platoformClient
 * @param {Object}   config                     layer-platform-client configuration object
 * @param {String}   config.appId               layer application ID
 * @param {Boolean}  config.enabled             whether or not to make requests to layer
 * @param {Number}   config.reqTimeout          time allowed for layer to respond
 * @param {String}   config.platformKey         the platform key used in the layer API
 * @returns {Object} layer platform client
 */
module.exports = function platformClient(config) {
  // Check for errors in the config
  if (!config) {
    throw new Error("layer-platform-client requires a config object");
  }

  if (!config.appId) {
    throw new Error("layer-platform-client config requires an appId");
  }

  if (!config.platformKey) {
    throw new Error("layer-platform-client config requires a platformKey");
  }

  if (!config.reqTimeout) {
    config.reqTimeout = 10 * 1000; // 10 seconds
  }

  var layerUrl = "https://api.layer.com";
  var basePath = "apps/" + config.appId;
  var standardHeaders = {
    "Accept": "application/vnd.layer+json; version=1.0",
    "Content-Type": "application/json",
    "Authorization": "Bearer " + config.platformKey
  };

  /*
   * Send a request to layer with the appropriate headers set
   * @param {Object} opts
   * @param {String} opts.method
   * @param {String} opts.path
   * @param {Object} opts.headers
   * @param {Object} opts.body
   */
  function makeRequest(opts) {
    if (opts === undefined) {
      opts = {};
    }

    if (!opts.method) {
      throw new Error("layer request method required");
    }
    if (!opts.path) {
      throw new Error("layer request path required");
    }
    opts.headers = opts.headers || {};

    if (opts.method === "patch") {
      opts.headers["Content-Type"] = "application/vnd.layer-patch+json";
    }

    var url = layerUrl + "/" + basePath + "/" + opts.path;
    var reqHeaders = combine(standardHeaders, opts.headers);

    if (config.enabled) {
      return got(url, {
        headers: reqHeaders,
        method: opts.method,
        body: JSON.stringify(opts.body)
      }).catch(function(err) {
        if (err.statusCode && err.statusCode >= 400) {
          return err.response;
        }
        else {
          throw err;
        }
      }).then(function(res) {
        res.origBody = res.body;
        res.body = JSON.parse(res.body);
        return res;
      });
    } else {
      return Bluebird.resolve({});
    }
  }

  /*
   * Create a conversation between recipients on Layer
   *
   * @param {Object}  body
   * @param {Array}   body.participants
   * @param {Boolean} body.distinct
   * @param {Object}  body.metadata
   *
   */
  function createConversation(body) {
    var method = "post";
    var path = "conversations";

    var form = schemas.createConversation.validate(body);

    if (!form.valid) {
      throw new ValidationError("layer conversation body not valid", form.errors);
    }

    return makeRequest({
      method: method,
      path: path,
      body: form.data
    });
  }

  /*
   * Update a conversation on Layer
   *
   * @param {Array}   body
   * @param {Object}  body[]
   * @param {String}  body[].operation
   * @param {String}  body[].property
   * @param {String}  body[].value
   */
  function updateConversation(id, body) {
    var method = "patch";
    var path = "conversations/" + id;
    var form = schemas.updateConversation.validate(body);

    if (!form.valid) {
      throw new ValidationError("layer updateConversation body not valid", form.errors);
    }

    return makeRequest({
      method: method,
      path: path,
      body: form.data
    });
  }

  /*
   * Get a conversation by id
   *
   * @param {String}  id
   */
  function getConversation(id) {
    var method = "get";
    var path = "conversations/" + id;
    return makeRequest({
      method: method,
      path: path
    });
  }


  /*
   * Update a conversation on Layer
   *
   * @param {String}  id
   * @param {Object}  body
   * @param {Object}  body.sender
   * @param {Object}  body.parts
   * @param {Object}  body.notification
   */
  function sendMessage(conversationId, body) {
    var method = "post";
    var path = "conversations/" + conversationId + "/messages";
    var form = schemas.sendMessage.validate(body);

    if (!form.valid) {
      throw new ValidationError("layer sendMessage body not valid", form.errors);
    }

    return makeRequest({
      method: method,
      path: path,
      body: form.data
    });
  }

  function getIdFromLayerUrl(layerId) {
    return layerId.replace("layer:///conversations/", "");
  }

  return {
    layerUrl: layerUrl,
    basePath: basePath,
    standardHeaders: standardHeaders,
    getIdFromLayerUrl: getIdFromLayerUrl,
    makeRequest: makeRequest,
    createConversation: createConversation,
    updateConversation: updateConversation,
    getConversation: getConversation,
    sendMessage: sendMessage
  };
};
