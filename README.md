Layer Platform Client
=====================

A light-weight wrapper for the Layer API.


Usage
-----

First, you'll need to create the client with the appropiate configuration and errors that you require.
```javascript
var LayerClient = require('layer-platform-client');
var config = require('./config');
var errors = require('./errors');

var layer = LayerClient(config, errors);
```

#### `makeRequest`
```javascript
config.layer.enabled = true;

layer.makeRequest({
    method: "get",
    path: "somepath"
}).then(function(res) {
    // Response

    console.log("Hey! A response!", res.body);
});
```

#### `createConversation`
```javascript
config.layer.enabled = true;

var convData = {
    // ...  
};

layer.createConversation(convData).then(function(res) {
    // Response

    console.log("Conversation response!", res.body);
});
```

#### `updateConversation`
```javascript
config.layer.enabled = true;

var newData = {
    // ...  
};

layer.updateConversation(1, newData).then(function(res) {
    // Response

    console.log("Conversation update!", res.body);
});
```

#### `sendMessage`
```javascript
config.layer.enabled = true;

var messageData = {
    // ...  
};

layer.sendMessage(1, messageData).then(function(res) {
    // Response

    console.log("Conversation update!", res.body);
});
```
